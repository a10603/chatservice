const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const Msg = require('./models/messages');

const jwt = require("jsonwebtoken");

const config = process.env;

const app = express();
app.use(cors());
app.use(express.json());
dotenv.config();
const mongoDB = process.env.MONGO_URI;
const http = require('http').createServer(app);
const io = require('socket.io')(http, {
    cors: {
        origin: '*'
    },
});


const connect = async () => {
    try {
        await mongoose.connect(mongoDB);
        console.log("Connected to mongoDB.");
    } catch (error) {
        console.log(error);
    }
};

mongoose.connection.on("disconnected", () => {
    console.log("mongoDB disconnected!");
});


app.get('/', (req, res) => {
    console.log("connected");
    res.send('chat started');
})

let userList = new Map();


io.on('connection', (socket) => {

    let userName = socket.handshake.query.userName;
    let token = socket.handshake.query.token;

    Msg.find().then((result) => {
        socket.emit('output-messages', result)
    })
    addUser(userName, socket.id);
    socket.nickname = userName;
    let query = socket.handshake.query;
    socket.on('join-room', (room) => {
        socket.join(room)
    });

    ///users should be connected before adding comments:
    socket.on('message', async (msg, room) => {

        if (token) {
            try {
                const decoded = await jwt.verify(token, config.TOKEN_KEY);
                const message = new Msg({ msg: msg, room: room, username: userName });
                await message.save();
                socket.to(room).emit('message-broadcast', { message: msg, userName: userName })
            } catch (err) {7
                socket.to(room).emit('message-broadcast', { message: "please connect", userName: "userName" })
            }
        }
    })

    socket.on('disconnect', (reason) => {
        removeUser(userName, socket.id);
    })
});


function addUser(userName, id) {
    if (!userList.has(userName)) {
        userList.set(userName, new Set(id));
    } else {
        userList.get(userName).add(id);
    }
}

function removeUser(userName, id) {
    if (userList.has(userName)) {
        let userIds = userList.get(userName);
        if (userIds.size == 0) {
            userList.delete(userName);
        }
    }
}


http.listen(4004, () => {
    connect();
    console.log('chat is running');
});